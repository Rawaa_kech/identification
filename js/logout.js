function sendFormLogout(ev) {
    ev.preventDefault();
    let url = 'services/logout.php';
    fetchFromJson(url)
    .then(processAnswer)
    .then(logOut); 
}



/**
 * 
 * @param {JSON} answer a requet result
 * @return null if the requet is not ok
 *         object else;
 * 
 */
function processAnswer(answer){
    if (answer.status == "ok")
        return answer.result;
    return null;
}

function logOut(){
    setLoginInterface();
}