window.addEventListener('load', setupListeners);



function setupListeners() {
    
    document.forms.formLogin.addEventListener('submit', sendFormLogin);

}

/**
 * 
 * @param {JSON} answer a requet result
 * @return null if the requet is not ok
 *         object else;
 */
function processAnswer(answer){
    if (answer.status == "ok")
        return answer.result;
    return null;
}


function sendFormLogin(ev) {
    ev.preventDefault();
    let args = new FormData(this);
    let queryString = new URLSearchParams(args).toString();
    let url = 'services/login.php?' + queryString;
    fetchFromJson(url , {method:'post',body:args})
    .then(processAnswer)
    .then(setCorrectInterface); 
}


function setCorrectInterface(user){
    if (user === null){
        setLoginInterface();
        let msgContainer = document.getElementById('message');
        msgContainer.textContent = "identifiants incorrect";
    }        
    else{
        setConnectedInterface(user) ;
        //let msgContainer = document.getElementById('message');
    }
}

function setLoginInterface(){
    let header = document.getElementsByTagName('header')[0];
    clearContent(header);
    header.appendChild(createForm());
}

function setConnectedInterface(user) {
    let header = document.getElementsByTagName('header')[0];
    let userNameContainer = document.createElement('span')
    userNameContainer.textContent = user.prenom + ' ' + user.nom; 
    let usersInfos = document.createElement('h1');
    usersInfos.appendChild(userNameContainer);
    clearContent(header);
    header.appendChild(usersInfos);
    let formulaireDeconnexion = document.createElement('form');
    formulaireDeconnexion.setAttribute('id', 'deconnexion');
    formulaireDeconnexion.setAttribute('action', '');
    let logoutButton = document.createElement('button');
    logoutButton.textContent = "deconnexion"; 
    formulaireDeconnexion.appendChild(logoutButton);
    header.appendChild(formulaireDeconnexion);
    formulaireDeconnexion.addEventListener('submit', sendFormLogout);
}

function clearContent(element){
    element.textContent = "";
}

function createForm(){
    let form = document.createElement('form');
    form.setAttribute('id', 'formLogin');
    let fieldset = document.createElement('fieldset');
    fieldset.appendChild(createLabel('Login', 'login'));
    fieldset.appendChild(createInput('text', 'login'));
    fieldset.appendChild(createLabel('Mot de passe', 'password'));
    fieldset.appendChild(createInput('password', 'password'));
    buuton = document.createElement('button');
    buuton.textContent = "OK";
    fieldset.appendChild(buuton);
    form.append(fieldset);
    div = document.createElement('div');
    div.setAttribute('id', 'message');
    form.appendChild(div);
    form.addEventListener('submit', sendFormLogin);
    return form;
}

function createLabel(referenceContent, forValue){
    let label = document.createElement('label');
    label.setAttribute('for', forValue);
    label.textContent = referenceContent;
    return label;
}

function createInput(type, name){
    let input = document.createElement('input');
    input.setAttribute('type', type);
    input.setAttribute('id', name);
    input.setAttribute('name', name);

    return input;
}